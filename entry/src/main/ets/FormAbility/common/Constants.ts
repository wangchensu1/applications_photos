/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class Constants {
    // string value
    static readonly FROM_CONTROLLER_MANAGER = 'form_controller_manager';
    static readonly PHOTOS_FORM_STORAGE_PREFIX = 'photos_form_';
    static readonly PHOTOS_FORM_CAMERA_NAME = 'Camera';
    static readonly PHOTOS_FORM_ALBUM_NAME_ALL = 'default_all';
    static readonly PHOTOS_FORM_OPERATION_MODE_NONE = 0;
    static readonly PHOTOS_FORM_OPERATION_MODE_DESTROY = 1;
    static readonly PHOTOS_FORM_OPERATION_MODE_UPDATE = 2;
    static readonly PHOTOS_FORM_OPERATION_MODE_EVENT = 3;
    static readonly PHOTOS_FORM_OPERATION_MODE_CALLBACK = 4;
    static readonly FORM_ID = 'formId_';
    static readonly DISPLAY_NAME = 'displayName_';
    static readonly ALBUM_NAME = 'albumName_';
    static readonly CURRENT_URL = 'currentUri_';
    static readonly INTERVAL_TIME = 'intervalTime_';
    static readonly CURRENT_INDEX = 'currentIndex_';
    static readonly FLEX_GROW = 2;
    static readonly DIALOG_BOTTOM_OFFSET: number = 12;
    static readonly NUMBER_2 = 2;
    static readonly NUMBER_3 = 3;
    static readonly NUMBER_4: number = 4;
    static readonly NUMBER_8: number = 8;
    static readonly NUMBER_12: number = 12;
    static readonly NUMBER_24: number = 24;
    static readonly FROM_PLAYBACK_INTERVAL = 'form_playback_interval';
    static readonly PHOTOS_FORM_DEFAULT_PERIOD = 30;
}