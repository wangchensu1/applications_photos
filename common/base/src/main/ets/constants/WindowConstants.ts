/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class WindowConstants {
    static readonly MAIN_WINDOW: string = 'mainWindow';
    static readonly TOP_BAR_SIZE: number = 56;
    static readonly TOOL_BAR_SIZE: number = 72;
    static readonly BOTTOM_TOOL_BAR_SIZE: number = 196;
    static readonly FILTER_BOTTOM_TOOL_BAR_SIZE: number = 232;
    // Grid Constants
    static readonly ACTION_BAR_HEIGHT: number = 56;
    static readonly TAB_BAR_WIDTH: number = 96;
    static readonly GRID_GUTTER: number = 2;
    static readonly GRID_IMAGE_SIZE: number = 256;
    static readonly GRID_MAX_SIZE_RATIO: number = 1.2;
    static readonly GRID_MIN_COUNT: number = 4;
    static readonly SCROLL_BAR_SIDE_MIN_GAP: number = 12;
    static readonly SCROLL_MARGIN: number = 24;
    static readonly SCROLL_BAR_VISIBLE_THRESHOLD: number = 50;
    static readonly CARD_ASPECT_RATIO: number = 0.75;
    static readonly TEXT_SIZE_SUB_TITLE1: number = 18; // ohos_id_text_size_sub_title1
    static readonly TEXT_SIZE_BODY2: number = 14; // ohos_id_text_size_body2
    static readonly TEXT_SIZE_SUB_TITLE2: number = 16; // ohos_id_text_size_sub_title2

    static readonly ALBUM_SET_NEW_ICON_SIZE: number = 22;
    static readonly ALBUM_SET_NEW_ICON_MARGIN: number = 8;
    static readonly ALBUM_SET_MARGIN: number = 24;
    static readonly ALBUM_SET_GUTTER: number = 12;
    static readonly ALBUM_SET_COVER_SIZE: number = 150;
}